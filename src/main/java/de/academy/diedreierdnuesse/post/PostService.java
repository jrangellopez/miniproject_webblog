package de.academy.diedreierdnuesse.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PostService {

    @Autowired
    private PostRepository postRepository;

    public void savePost(PostDTO postDTO){

        Post post = postRepository.findPostById(postDTO.getId());
        post.setTitle(postDTO.getTitle());
        post.setText(postDTO.getText());

        postRepository.save(post);

    }



}
