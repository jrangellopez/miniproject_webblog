package de.academy.diedreierdnuesse.post;

import javax.validation.constraints.NotEmpty;

public class PostDTO {

    private Long id;
    @NotEmpty
    private String title;
    @NotEmpty
    private String text;
    @NotEmpty
    private String imagePath = "img/" ;


    public PostDTO(){}


    public PostDTO(Long id, String title, String text) {
        this.id = id;
        this.title = title;
        this.text = text;
    }

    public PostDTO(Long id, String title, String text, String imagePath) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.imagePath = imagePath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
