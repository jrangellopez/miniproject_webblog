package de.academy.diedreierdnuesse.post;

import de.academy.diedreierdnuesse.comment.Comment;
import de.academy.diedreierdnuesse.comment.CommentRepository;
import de.academy.diedreierdnuesse.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class PostController {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostService postService;


    @GetMapping("/posts")
    public String sortDesc(Model model) {
        model.addAttribute("posts", postRepository.findAllByOrderByCreationDateTimeDesc());
        return "posts";
    }

    @GetMapping("/createPost")
    public String createPostForm(Model model) {
        model.addAttribute("post", new PostDTO());
        return "createPost";
    }

    @PostMapping("/createPost")
    public String createPost(@ModelAttribute("post") @Valid PostDTO postDTO,
                             BindingResult bindingResult,
                             @ModelAttribute("currentUser") User currentUser) {

        if (bindingResult.hasErrors()) {
            return "/createPost";
        }

        Post post = new Post(postDTO.getTitle(), postDTO.getText(), currentUser, postDTO.getImagePath());

        postRepository.save(post);

        return "redirect:/posts";
    }

    @GetMapping("/editPost/{postId}")
    public String editPostForm(Model model, @PathVariable(value = "postId") Long postId) {
        Post post = postRepository.findPostById(postId);
        model.addAttribute("postToEdit", new PostDTO(post.getId(), post.getTitle(), post.getText()));
        return "/editPost";
    }

    @PostMapping("/editPost")
    public String editPost(@ModelAttribute("postToEdit") @Valid PostDTO postDTO,
                           BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "/editPost";
        }

        postService.savePost(postDTO);
        return "redirect:/posts";
    }

    @GetMapping("/deletePost/{postId}")
    public String deletePostForm(Model model, @PathVariable(value = "postId") Long postId) {
        Post post = postRepository.findPostById(postId);
        model.addAttribute("postToDelete", post);
        return "deletePost";
    }

    @PostMapping("/deletePost/{postId}")
    public String deletePost(Model model, @PathVariable(value = "postId") Long postId) {
        Post post = postRepository.findPostById(postId);
        List<Comment> comments = post.getComment();

        if (comments.size() > 0) {
            for (Comment comment : comments) {
                commentRepository.delete(comment);
            }
        }

        postRepository.delete(post);

        return "redirect:/posts";
    }


}
