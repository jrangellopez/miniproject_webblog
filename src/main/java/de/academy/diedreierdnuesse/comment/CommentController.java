package de.academy.diedreierdnuesse.comment;

import de.academy.diedreierdnuesse.post.Post;
import de.academy.diedreierdnuesse.post.PostRepository;
import de.academy.diedreierdnuesse.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class CommentController {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;

    @GetMapping("/createComment/{postId}")
    public String createComment(Model model, @PathVariable(value = "postId") Long postId){
        model.addAttribute("comment", new CommentDTO());
        model.addAttribute("postId", postId);
        return "/createComment";
    }

    @PostMapping("/createComment/{postId}")
    public String createComment(@PathVariable(value = "postId") Long postId,
                                @ModelAttribute("comment") @Valid CommentDTO commentDTO,
                                BindingResult bindingResult,
                                @ModelAttribute("currentUser") User currentUser){

        /*if(commentDTO.getText().equals("")){
            bindingResult.addError(new FieldError("comment", "emptyComment", "Du hast kein Kommentar geschrieben"));
        }*/

        if(bindingResult.hasErrors()){
            bindingResult.addError(new FieldError("comment", "emptyText", "wemcöe"));
            return "/createComment";
        }

        Comment comment = new Comment(commentDTO.getText(), currentUser);
        Post post = postRepository.findPostById(postId);
        post.getComment().add(comment);

        commentRepository.save(comment);
        return "redirect:/posts";
    }

     @GetMapping("/deleteComment/{commentId}")
     public String deleteCommentForm(Model model, @PathVariable(value = "commentId") Long commentId){
         Comment comment = commentRepository.findCommentById(commentId);
         model.addAttribute("commentToDelete", comment);
         return "/deleteComment";
     }

    @PostMapping("/deleteComment/{commentId}")
    public String deleteComment(Model model, @PathVariable(value = "commentId") Long id){
         Comment comment = commentRepository.findCommentById(id);
         commentRepository.delete(comment);

        return "redirect:/posts";
    }

}
