package de.academy.diedreierdnuesse.user;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    boolean existsByUsername(String username);

    Optional<User> findFirstByUsernameAndPassword(String username, String password);

    User findFirstById(Long id);

    Optional<User> findFirstByUsername(String username);

    List<User> findAllByOrderByUsername();

}
