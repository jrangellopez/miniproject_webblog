package de.academy.diedreierdnuesse.user;

import de.academy.diedreierdnuesse.comment.Comment;
import de.academy.diedreierdnuesse.comment.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CommentRepository commentRepository;

    @GetMapping("/showUsers")
    public String showUsers(Model model){
        model.addAttribute("users", userRepository.findAllByOrderByUsername());
        model.addAttribute("userDTO", new UserDTO());
        return "showUsers";
    }

    @PostMapping("giveAdminRights")
    public String giveAdminRights(Model model,
                                  @ModelAttribute("userDTO") @Valid UserDTO userDTO,
                                  BindingResult bindingResult){

        Optional<User> optionalUser = userRepository.findFirstByUsername(userDTO.getUsername());

        if(!optionalUser.isPresent()){
            bindingResult.addError(new FieldError("userDTO", "username", "Dieser Benutzername existiert nicht"));
        }

        if(optionalUser.isPresent() && optionalUser.get().isAdmin()){
            bindingResult.addError(new FieldError("userDTO", "username", "Dieser Benutzername hat schon Adminrechte"));
        }

        if(bindingResult.hasErrors()){
            model.addAttribute("users", userRepository.findAllByOrderByUsername());
            return "showUsers";
        }

        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            user.setAdmin(true);
            userRepository.save(user);
        }

        return "redirect:showUsers";
    }


    @PostMapping("deleteUser")
    public String deleteUser(Model model,
                                  @ModelAttribute("userDTO") @Valid UserDTO userDTO,
                                  BindingResult bindingResult){

        Optional<User> optionalUser = userRepository.findFirstByUsername(userDTO.getUsername());

        if(!optionalUser.isPresent()){
            bindingResult.addError(new FieldError("userDTO", "username", "Dieser Benutzername existiert nicht"));
        }

        if(bindingResult.hasErrors()){
            model.addAttribute("users", userRepository.findAll());
            return "showUsers";
        }

        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            Optional<List<Comment>> comments = commentRepository.findCommentByUser(user);

            if(comments.isPresent()){
                for(Comment comment : comments.get()){
                    commentRepository.delete(comment);
                }
            }

            userRepository.delete(user);
        }

        return "redirect:showUsers";
    }



}
