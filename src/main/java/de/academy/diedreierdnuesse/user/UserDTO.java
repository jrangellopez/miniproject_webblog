package de.academy.diedreierdnuesse.user;

import javax.validation.constraints.NotEmpty;

public class UserDTO {

    @NotEmpty
    private String username = "";

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
