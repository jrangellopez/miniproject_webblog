package de.academy.diedreierdnuesse;

import de.academy.diedreierdnuesse.comment.Comment;
import de.academy.diedreierdnuesse.comment.CommentRepository;
import de.academy.diedreierdnuesse.post.PostController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PostControllerTest {


    @Mock
    private CommentRepository commentRepository;

    @InjectMocks
    private PostController postController;

    @Before
    public void mocksVorbereiten() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findCommentById() {

        Comment comment = new Comment();
        Optional<Comment> beitragOptional = Optional.of(comment);

        //Regel festlegen


        Mockito.when(
                commentRepository.findCommentById((long) 1)).thenReturn(comment);

//Meine Implementierung testen

//        Optional<Comment> comment1= postService.save();


    }
}

